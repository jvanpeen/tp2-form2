import { ref } from 'vue'
import Schema from 'async-validator'

export function useForm(options) {
  const form = ref(options.form)

  const formErrors = ref(options.formErrors)

  // const validator = new Schema({ validator: options })
  const validator = new Schema(options.validator)

  function validate() {
    validator.validate(form.value, (errors) => {
      formErrors.value = {}

      for (const error of errors) {
        console.log(error)
        formErrors.value[error.field] = error.message
      }

      if (Object.keys(formErrors.value).length) {
        return
      }

      if (form.value) {
        console.log('Form Value', form.value)
      } else {
        console.log('erreur form')
      }

      return
    })
  }

  return { form, formErrors, validator, validate }
}
